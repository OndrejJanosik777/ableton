import React, { Component } from 'react';
import { useState } from 'react';
import './ItemFeature.scss';

const ItemFeature = (props) => {
    const getColor = (type) => {
        if (type === 'Loop')
            return 'feature-type feature-light-blue';
        if (type === 'Tutorials')
            return 'feature-type feature-dark-blue';
        if (type === 'Downloads')
            return 'feature-type feature-orange';
        if (type === 'Artists')
            return 'feature-type feature-yellow';
        if (type === 'Videos')
            return 'feature-type feature-viollet';
        else
            return 'feature-type feature-black';
    }

    // component state
    const [typeClass, setTypeClass] = useState(getColor(props.type))

    return (<div className='item-feature'>
        <img className='image' src={props.image} />
        <div className={typeClass}>{props.type}</div>
        <div className='feature-text'>{props.text}</div>
    </div>);
}

export default ItemFeature;