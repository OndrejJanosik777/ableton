import React, { Component } from 'react';
import { useState } from 'react';
import './NavBar.scss';
import menuHorizontal from '../../assets/svg/menu-horizontal.svg';
import menuVertical from '../../assets/svg/menu-vertical.svg';

const NavBar = () => {
    // component state
    const [lowerPart, setLowerPart] = useState('lower-part-hidden');

    const handlePlusClick = () => {
        console.log('extending...');

        let status = document.getElementById('nav-bar-extendable').innerHTML;

        if (status === 'More +') {
            document.getElementById('nav-bar-extendable').innerHTML = 'More -';
            setLowerPart('lower-part-extended');
        }

        if (status === 'More -') {
            document.getElementById('nav-bar-extendable').innerHTML = 'More +';
            setLowerPart('lower-part-hidden');
        };
    }

    return (
        <div className='nav-bar'>
            <div className='nav-bar-upper'>
                <div className='nav-bar-left'>
                    <img src={menuHorizontal} />
                    <img src={menuVertical} />
                    <ul className='nav-bar-items'>
                        <li className='nav-bar-item'>Live</li>
                        <li className='nav-bar-item'>Push</li>
                        <li className='nav-bar-item'>Link</li>
                        <li className='nav-bar-item'>Shop</li>
                        <li className='nav-bar-item'>Packs</li>
                        <li className='nav-bar-item'>Help</li>
                        <li id='nav-bar-extendable'
                            className='nav-bar-item nav-bar-extendable'
                            onClick={handlePlusClick}
                        >More +</li>
                    </ul>
                </div>
                <div className='nav-bar-right'>
                    <ul className='nav-bar-items'>
                        <li className='nav-bar-item'>Try Live for free</li>
                        <li className='nav-bar-item '>Log in or register</li>
                    </ul>
                </div>
            </div>
            <div className='lower-part'>
                <div className={lowerPart}>
                    <div className='title'>More on Ableton.com:</div>
                    <ul className='nav-bar-items'>
                        <li className='nav-bar-item'>Blog</li>
                        <li className='nav-bar-item'>Ableton for the Classroom</li>
                        <li className='nav-bar-item'>Ableton for Collegues and Universities</li>
                        <li className='nav-bar-item'>Certified Training</li>
                        <li className='nav-bar-item'>About Ableton</li>
                        <li className='nav-bar-item'>Jobs</li>
                        <li className='nav-bar-item'>Apprenticeships</li>
                    </ul>
                    <div className='title'>More from Ableton:</div>
                    <ul className='nav-bar-items'>
                        <li className='nav-bar-item'>
                            <div className='title'>Loop</div>
                            <div className='text'>
                                Watch Talks, Performances and Features from Ableton's Summit for Music Makers.
                            </div>
                        </li>
                        <li className='nav-bar-item'>
                            <div className='title'>Learning Music</div>
                            <div className='text'>
                                Learn the fundamentals of music making right in your browser.
                            </div>
                        </li>
                        <li className='nav-bar-item'>
                            <div className='title'>Learning Synths</div>
                            <div className='text'>
                                Get started with synthesis using a web-based synth and accompanying lessons.
                            </div>
                        </li>
                        <li className='nav-bar-item'>
                            <div className='title'>Making Music</div>
                            <div className='text'>
                                Some tips from 74 Creative Strategies for Electronic Producers.
                            </div>
                        </li>
                        <li className='nav-bar-item'></li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

export default NavBar; 