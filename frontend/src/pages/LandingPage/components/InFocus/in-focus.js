import React, { Component } from 'react';
import './in-focus.scss';
// components
import ItemFeature from '../../../../components/ItemFeature/ItemFeature';
import image1 from '../../../../assets/jpg/bl_learning_synth.jpg__800.0x401.0_q85_subsampling-2.jpg';
import image2 from '../../../../assets/jpg/abl265_tutorialelphnt7.jpg__800.0x400.0_q85_subsampling-2.jpg';
import image3 from '../../../../assets/jpg/2022_05_30_23_16_29_Music_production_with_Live_and_Push_Ableton.jpg';

const InFocus = () => {
    return (<div className='in-focus'>
        <div className='in-focus-top'>
            <h1 className='header'>In Focus: Synthesis</h1>
        </div>
        <div className='in-focus-body'>
            <ItemFeature image={image1} type={'Tutorials'} text={'New in Learning Synths: Export to Live, Record Your Creations, and More'} />
            <ItemFeature image={image2} type={'Artists'} text={'Arushi Jain: I’m the Space in Between'} />
            <ItemFeature image={image3} type={'Videos'} text={'Complex Drum Programming with Ned Rush'} />
        </div>
    </div>);
}

export default InFocus;