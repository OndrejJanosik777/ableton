import React, { Component } from 'react';
import ReactPlayer from 'react-player'
import image1 from '../../../../assets/jpg/blog_xlr8r_jinku_jjy.jpg__3333.0x1667.0_q85_subsampling-2.jpg';
import image2 from '../../../../assets/jpg/fractal_filters__dsc0540.jpg__2000.0x1333.0_q85_subsampling-2.jpg';
import './middle-part-top.scss';

const MiddlePartTop = () => {
    return (
        <div className='middle-part-top'>
            <div className='video-top-left'>
                <ReactPlayer width={'100%'} height={'80%'} controls url='https://www.youtube.com/watch?v=18r2cE-hpj0' />
            </div>
            <img className='image1' src={image1} />
            <img className='image2' src={image2} />
        </div>
    );
}

export default MiddlePartTop;