import React, { Component } from 'react';
import './the-latest.scss';
// components
import ItemFeature from '../../../../components/ItemFeature/ItemFeature';
import image1 from '../../../../assets/jpg/blog_hero_image_-_option_2.jpg__3333.0x1667.0_q85_subsampling-2.jpg';
import image2 from '../../../../assets/jpg/blog_image_1.jpg__8256.0x4128.0_q85_subsampling-2.jpg';
import image3 from '../../../../assets/jpg/323_tennyson4.jpg__800.0x400.0_q85_subsampling-2.jpg';

const TheLatest = () => {
    return (<div className='the-latest'>
        <div className='the-latest-top'>
            <h1 className='header'>The latest from Ableton</h1>
            <ul className='items'>
                <li className='item'>All posts</li>
                <li className='item'>Artists</li>
                <li className='item'>News</li>
                <li className='item'>Downloads</li>
                <li className='item'>Tutorials</li>
                <li className='item'>Videos</li>
                <li className='item'>Loop</li>
            </ul>
        </div>
        <div className='the-latest-body'>
            <ItemFeature image={image1} type={'Loop'} text={'Loop: Five Perspectives on Handling Criticism'} />
            <ItemFeature image={image2} type={'Downloads'} text={'The Climate Soundtrack: What Does the Future Sound Like?'} />
            <ItemFeature image={image3} type={'Artists'} text={'Tennyson: Storytelling With Production'} />
        </div>
    </div>);
}

export default TheLatest;