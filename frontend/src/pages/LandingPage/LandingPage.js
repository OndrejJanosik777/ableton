import React, { Component } from 'react';
import { Player } from 'video-react';
import ReactPlayer from 'react-player'
// import components
import NavBar from '../../components/NavBar/NavBar';
import MiddlePartTop from './components/MiddlePartTop/middle-part-top';
import TheLatest from './components/TheLatest/the-latest';
import InFocus from './components/InFocus/in-focus';
import image1 from '../../assets/jpg/blog_xlr8r_jinku_jjy.jpg__3333.0x1667.0_q85_subsampling-2.jpg';
import image2 from '../../assets/jpg/fractal_filters__dsc0540.jpg__2000.0x1333.0_q85_subsampling-2.jpg';
import './LandingPage.scss';
// import videos
// import video1 from 'https://www.youtube.com/watch?v=18r2cE-hpj0';

const LandingPage = () => {
    return (
        <div className='landing-page'>
            <div className='middle-part'>
                <NavBar />
                <MiddlePartTop />
                <TheLatest />
                <InFocus />
            </div>
        </div>);
}

export default LandingPage;